# README #

### What is this repository for? ###

* Location of Source Files for FYP Project

### Rules ###

* Please read up the Instruction Manual, it is posted on NYP AES.
* Please try **NOT** use this outside of the sterile VM environment.  Unless you know exactly what you are doing.
* Please do **NOT** use this codes for your own personal gain.

### Set Up ###

* Set up an account.
* Contact me for any details

### Contact ###

* Glen Goh
    * Email: hmggoh001@mymail.sim.edu.sg
    * phone: On request, please email me to get my number.  In that email, state your name, admin number and seat number.


> This is README is created by Glen Goh on 25/10/2016 at 1131.

> Last update: 25/10/2016 at 1148
